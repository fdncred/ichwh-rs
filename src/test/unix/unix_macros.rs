//! Macros for the unix tests
//!
//! Each of these macros is some kind of shortcut for creating files and directories. Using these
//! ensures that the tests are isolated and can be easily cleaned up at the end.

/// Creates a file and sets it as executable.
#[macro_export]
macro_rules! create_exec_file {
    ($dir:expr, $filename:literal) => {
        async {
            use {async_std::fs::Permissions, std::os::unix::fs::PermissionsExt};
            let file = create_file!($dir, $filename).await;
            file.set_permissions(Permissions::from_mode(0o777))
                .await
                .unwrap();
        }
    };
}

/// Creates a file and sets it as read/write, not executable.
#[macro_export]
macro_rules! create_noexec_file {
    ($dir:expr, $filename:literal) => {
        async {
            use {async_std::fs::Permissions, std::os::unix::fs::PermissionsExt};
            let file = create_file!($dir, $filename).await;
            file.set_permissions(Permissions::from_mode(0o644))
                .await
                .unwrap();
        }
    };
}

/// Creates a symlink. `dst` -> `src`
#[macro_export]
macro_rules! create_symlink {
    ($dir:expr, $src:literal, $dst:literal) => {
        async {
            ::async_std::os::unix::fs::symlink(
                path!($dir.to_str().unwrap(), $src),
                path!($dir.to_str().unwrap(), $dst),
            )
            .await
            .unwrap();
        }
    };

    ($src_dir:expr, $src:literal, $dst_dir:expr, $dst:literal) => {
        async {
            ::async_std::os::unix::fs::symlink(
                path!($src_dir.to_str().unwrap(), $src),
                path!($dst_dir.to_str().unwrap(), $dst),
            )
            .await
            .unwrap();
        }
    };
}

/// Create a directory with executable permissions
#[macro_export]
macro_rules! create_exec_dir {
    ($dir:expr, $dirname:literal) => {
        async {
            use {async_std::fs::Permissions, std::os::unix::fs::PermissionsExt};
            let path = path!($dir.to_str().unwrap(), $dirname);
            ::async_std::fs::create_dir_all(&path).await.unwrap();
            ::async_std::fs::set_permissions(&path, Permissions::from_mode(0o777))
                .await
                .unwrap();
            path
        }
    };
}
