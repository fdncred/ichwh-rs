mod common_macros;

#[cfg(unix)]
mod unix;

#[cfg(windows)]
mod windows;
