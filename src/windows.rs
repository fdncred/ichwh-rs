//! Miscellaneous helper functions for windows

use {
    crate::{IchwhError, IchwhResult},
    async_std::{
        fs::DirEntry,
        path::{Path, PathBuf},
    },
};

// A file is executable if it has an executable extension.
//
// A file `a` can be expanded to `a.<ext>`, where `<ext>` is any executable
// extension. For example, invoking `.\a` will look for files named `a.bat`,
// `a.cmd`, etc. Invoking `.\a.bat` will first look for `a.bat`, then
// `a.bat.bat`, then `a.bat.cmd`, etc.
//
// The executable extensions are defined by the %PATHEXT% environment variable
// In a directory containing `a.cmd` and `a.bat`,
//
// A symlink is executable if:
// * the file it points to is executable
// * AND the symlink has an executable extension

/// Reads the `PATH` environment variable and splits it up into a list of directories.
///
/// # Errors
///
/// * `PATH` is not defined
pub fn split_path_env() -> IchwhResult<Vec<PathBuf>> {
    let path_var = std::env::var_os("PATH").ok_or(IchwhError::PathNotDefined)?;
    Ok(std::env::split_paths(&path_var).map(Into::into).collect())
}

/// Checks whether or not a file is executable.
///
/// # Errors
///
/// * An IO error occurs
pub async fn is_executable(file: &DirEntry) -> IchwhResult<bool> {
    let file_type = file.metadata().await?.file_type();

    // If the entry isn't a file, it cannot be executable
    if !(file_type.is_file() || file_type.is_symlink()) {
        return Ok(false);
    }

    if let Some(extension) = file.path().extension() {
        let exts = pathext()?;

        Ok(exts
            .iter()
            .any(|ext| extension.to_string_lossy().eq_ignore_ascii_case(ext)))
    } else {
        Ok(false)
    }
}

/// Checks whether or not a file is executable.
///
/// # Errors
///
/// * An IO error occurs
pub async fn is_executable_path<P: AsRef<Path>>(path: P) -> IchwhResult<Option<PathBuf>> {
    let actual_path = if path.as_ref().is_file().await {
        // A file exists at the path, so we continue with it.
        path.as_ref().to_path_buf()
    } else {
        // The file doesn't exist, so we append each extension in PATHEXT to the
        // filename until we find a matching file
        let exts = pathext()?;

        // See issue #3: Parent may not exist for paths like `..` or `../..`
        if let (Some(parent_dir), Some(filename)) =
            (path.as_ref().parent(), path.as_ref().file_name())
        {
            let filename = filename.to_owned();

            let mut rtn = None;

            for ext in exts {
                let mut filename = filename.clone();
                filename.push(".");
                filename.push(&ext);
                let path_to_check = parent_dir.join(Path::new(&filename));
                if path_to_check.is_file().await {
                    // It's a file (or a symlink to one), and we know that it has an
                    // executable extension.
                    rtn = Some(path_to_check);
                    break;
                }
            }

            if let Some(rtn) = rtn {
                rtn
            } else {
                return Ok(None);
            }
        } else {
            return Ok(None);
        }
    };

    let file_type = actual_path.metadata().await?.file_type();

    // If the entry isn't a file, it cannot be executable
    if !file_type.is_file() {
        return Ok(None);
    }

    if let Some(extension) = actual_path.extension() {
        // Check the extension is in `%PATHEXT%`
        let exts = pathext()?;

        if exts
            .iter()
            .any(|ext| extension.to_string_lossy().eq_ignore_ascii_case(ext))
        {
            Ok(Some(actual_path.canonicalize().await?))
        } else {
            Ok(None)
        }
    } else {
        Ok(None)
    }
}

/// Checks whether or not a filename that may or may not have an extension matches the name of the
/// given file.
pub fn filename_matches(bin: &str, file: &DirEntry) -> bool {
    if Path::new(bin).extension().is_some() {
        // `bin` has an extension, so the whole name has to match the entry name
        bin.eq_ignore_ascii_case(&file.file_name().to_string_lossy())
    } else {
        // `bin` does not have an extension, so it just has to match the entry's name w/o its extension
        if let Some(file_stem) = file.path().file_stem() {
            bin.eq_ignore_ascii_case(&file_stem.to_string_lossy())
        } else {
            false
        }
    }
}

/// Get the PATHEXT contents
///
/// TODO: use lazy_static for this
/// TODO: Define an error type for PATHEXT not defined
pub fn pathext() -> IchwhResult<Vec<String>> {
    Ok(std::env::var_os("PATHEXT")
        .ok_or(IchwhError::PathextNotDefined)?
        .to_string_lossy()
        .split(';')
        // Cut off the leading '.' character
        .map(|ext| ext[1..].to_string())
        .collect::<Vec<_>>())
}
